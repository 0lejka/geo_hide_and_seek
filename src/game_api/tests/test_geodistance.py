import math
import pytest
from unittest import mock
from .. import views


@pytest.fixture
def fake_radius():
    with mock.patch.object(views, 'EARTH_RADIUS', new=1) as fake:
        yield fake


def test_get_distance__same_coordinates():
    coords = (20, 20)
    distance = views.get_distance(coords, coords)
    assert distance == 0


@mock.patch.object(views, 'EARTH_RADIUS', new=1)
def test_get_distance__equator(fake_radius):
    coords1 = (0, 20)
    coords2 = (0, -160)
    distance = views.get_distance(coords1, coords2)
    assert abs(distance - math.pi*fake_radius) < 0.00001


@mock.patch.object(views, 'EARTH_RADIUS', new=1)
def test_get_distance__poles(fake_radius):
    coords1 = (-90, 20)
    coords2 = (90, -160)
    distance = views.get_distance(coords1, coords2)
    assert abs(distance - math.pi*fake_radius) < 0.00001


@mock.patch.object(views, 'EARTH_RADIUS', new=6371)
def test_get_distance__isome_points(fake_radius):
    coords1 = (58.5, -3)
    coords2 = (45.4152751, 36.28)
    distance = views.get_distance(coords1, coords2)
    assert abs(distance - 3000) < 0.00001
