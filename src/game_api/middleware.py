import os
from aiogmaps import Client


API_KEY = os.environ.get('GOOGLE_API_KEY', '')


async def redis_handler(app, handler):
    async def middleware(request):
        if request.path.startswith('/static/') or request.path.startswith('/_debugtoolbar'):
            response = await handler(request)
            return response

        request.redis = app.redis_pool
        response = await handler(request)
        return response
    return middleware


async def google_maps_handler(app, handler):
    async def middleware(request):

        if request.path.startswith('/static/') or request.path.startswith('/_debugtoolbar'):
            response = await handler(request)
            return response

        async with Client(API_KEY) as client:
            request.gmaps_client = client
            response = await handler(request)
            return response
    return middleware


