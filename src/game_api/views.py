import json
import math
from aiohttp import web
from aiohttp_session import get_session

ARENA_PLAYER = 'arena_player'
ARENA = 'arena'
ARENA_SIZE = 2
ROUND_TIME = 60
WIN_DISTANCE = 2
EARTH_RADIUS = 6371000


async def index(request):
    return web.Response(text='Hello Aiohttp!')


async def arena_hide(request):
    res = {}
    data = await request.json()

    location = data.get('locationTitle')
    if not location:
        raise web.HTTPBadRequest()

    geocode_response = await request.gmaps_client.geocode(location)
    location_coords = geocode_response[0]['geometry']['location']
    coords_str = (str(location_coords['lat']), str(location_coords['lng']))
    session = await get_session(request)
  
    player_num = await request.redis.incr('player_seq')
    arena_id = (player_num - 1) // ARENA_SIZE
    await request.redis.hset(f'arena:{arena_id}', str(player_num), 40000)
    await request.redis.hset(
        f'locations:{arena_id}', player_num, ','.join(coords_str))
    if not player_num % ARENA_SIZE:
        await request.redis.expire(f'arena:{arena_id}', ROUND_TIME)
    arena_members = await request.redis.hgetall(f'arena:{arena_id}')

    session[ARENA_PLAYER] = player_num
    session[ARENA] = arena_id
    
    res['player_id'] = player_num

    return web.Response(text=json.dumps(res))


def get_vector(coords):
    latitude, longitude = coords
    z = math.sin(latitude / 180* math.pi)
    xy = math.cos(latitude / 180* math.pi)
    x = xy * math.cos(longitude / 180* math.pi)
    y = xy * math.sin(longitude / 180* math.pi)
    return (x, y, z)


def get_distance(coords1, coords2):
    """
    We know that Earth has apioid shape but we are considering it as a sphere
    """
    vector1 = get_vector(coords1)
    vector2 = get_vector(coords2)
    scalar_mult = sum(i*j for i, j in zip(vector1, vector2))
    angle = math.acos(min(1.0, scalar_mult))
    return angle * EARTH_RADIUS


async def get_country_info(gmaps_client, coords):
    address_response = await gmaps_client.reverse_geocode(
        coords, result_type=('country', 'locality')
    )
    for address in address_response:
        for component in address['address_components']:
            if 'country' in component['types']:
                return component['long_name']


async def arena_seek(request):
    res = {}
    data = await request.json()

    location = data.get('locationTitle')
    if not location:
        raise web.HTTPBadRequest()

    geocode_response = await request.gmaps_client.geocode(location)
    location_info = geocode_response[0]['geometry']['location']
    location_coords = (location_info['lat'], location_info['lat'])

    session = await get_session(request)
    player_num = session.get(ARENA_PLAYER)
    arena_id = session.get(ARENA)
    
    res['player_id'] = player_num

    arena_members = await request.redis.hgetall(f'locations:{arena_id}')
    arena_members = {int(k): [float(j) for j in v.split(b',')] for k, v in arena_members.items()}
    for player, coords in arena_members.items():
        if player != player_num:
            distance = get_distance(location_coords, coords) // 1000

            if distance < WIN_DISTANCE:
                res['status'] = 'win'
                res['winner'] = player_num
                res['looser'] = player
                return web.Response(text=json.dumps(res))

            country = await get_country_info(request.gmaps_client, coords)

            await request.redis.hset(f'arena:{arena_id}', str(player_num), distance)

    return web.Response(text=json.dumps(res))


async def arena_state(request):
    res = {}
    session = await get_session(request)
    player_num = session.get(ARENA_PLAYER)
    arena_id = session.get(ARENA)
  
    arena_members = await request.redis.hgetall(f'arena:{arena_id}')
    player_location = await request.redis.hget(f'locations:{arena_id}', player_num)
    
    res['playerCoords'] = dict(zip(('lat', 'lng'), [float(i) for i in player_location.split(b',')]))
    res['arena'] = [
        {'player_id': k.decode('utf-8'), 'distance': v.decode('utf-8')}
        for k, v in arena_members.items()
    ]

    if len(arena_members) < ARENA_SIZE:
        res['status'] = 'wait'
    else:
        res['status'] = 'game'

    return web.Response(text=json.dumps(res))
