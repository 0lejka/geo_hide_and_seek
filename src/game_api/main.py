import asyncio
import os
from aiohttp import web
import aioredis
import aiohttp_session
import aiohttp_session.redis_storage

from routes import setup_routes
from middleware import redis_handler, google_maps_handler


async def make_redis_pool():
    redis_address = (
        os.environ.get('REDIS_HOST', '127.0.0.1'),
        os.environ.get('REDIS_PORT', '6379')
    )

    pool = await aioredis.create_redis_pool(
        redis_address,
    )
    await pool.set('player_seq', 0)

    return pool


def make_app():
    loop = asyncio.get_event_loop()
    redis_pool = loop.run_until_complete(make_redis_pool())
    storage = aiohttp_session.redis_storage.RedisStorage(redis_pool)
    session_middleware = aiohttp_session.session_middleware(storage)

    app = web.Application(
        middlewares=[session_middleware, redis_handler, google_maps_handler]
    )
    app.redis_pool = redis_pool
    setup_routes(app)
    return app


web.run_app(make_app())
