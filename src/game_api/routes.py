import views

def setup_routes(app):
    app.router.add_get('/', views.index)
    app.router.add_get('/api/', views.index)
    app.router.add_get('/api/arena/', views.arena_state)
    app.router.add_post('/api/arena/hide/', views.arena_hide)
    app.router.add_post('/api/arena/seek/', views.arena_seek)
