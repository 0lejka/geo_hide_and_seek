import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

class InlineForm extends Component {
  state = {value: ''}
  initialState = {value: ''}
  
  handleChange = event => {
    const { name, value } = event.target

    this.setState({
      [name]: value,
    })
  }

  submitForm = event => {
    this.props.handleSubmit(this.state);
    this.setState(this.initialState);
    event.preventDefault();
  }

  render() {
    const value = this.state.value;
  
    return (
      <div>
      <Form onSubmit={this.submitForm} inline>
	  <Form.Label htmlFor="inlineFormInputName2" srOnly>
	    Value
	  </Form.Label>
	  <Form.Control
                type="text"
                name="value"
                value={value}
                onChange={this.handleChange}
          />
          <Button type="submit" variant="outline-success">
            Submit!
          </Button>
      </Form>
      </div>
    );
  }

}

export default InlineForm
