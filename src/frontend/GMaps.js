import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker} from 'google-maps-react';

const mapStyles = {
  width: '100%',
  height: '100%'
};

export class GMaps extends Component {
  render() {
    return (
      <Map
        google={this.props.google}
        zoom={5}
        style={mapStyles}
        initialCenter={this.props.coords}
        center={this.props.coords}
      >
	    <Marker position={this.props.coords} />
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyDZYJBWXtulBFQ5hOYTMUOou6hQM3v68jY'
})(GMaps);
