import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import InlineForm from './Form';

class LocationModal extends Component {
	     
  render() {
    return (
	<Modal show={this.props.show}>
          <Modal.Header>
            <Modal.Title>Choose where to hide</Modal.Title>
          </Modal.Header>
          <Modal.Body>
	    <InlineForm handleSubmit={ this.props.handleSubmit } />
	  </Modal.Body>
        </Modal>
    )
  }
}

export default LocationModal
