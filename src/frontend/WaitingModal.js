import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import Table from './Table';

class WaitingModal extends Component {
	     
  render() {
    return (
	<Modal show={this.props.show}>
          <Modal.Header>
            <Modal.Title>We are waiting for participants</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Table rowData={this.props.tableData} />
	  </Modal.Body>
        </Modal>
    )
  }
}

export default WaitingModal
