import React, { Component } from 'react';
import logo from './logo.svg';
import InlineForm from './Form';
import LocationModal from './LocationModal';
import WaitingModal from './WaitingModal';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import GMaps from './GMaps';
import Table from './Table';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends Component {
  state = {
      arena: [],
      player_id: null,
      players: {},
      state: 'choose_location',
      playerCoords: {
         lat: -1.2884,
         lng: 36.8233
      }
  }

  joinArena = formData => {
    fetch(
	'http://localhost/api/arena/hide/',
	{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              locationTitle: formData.value
            })
          })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            state: 'wait'
	  });
	  setTimeout(() => this.tick(), 1000);
        },
        (error) => {
          this.setState({
	      state: 'choose_location'
          });
        }
      )
  }

  makeTurn = formData => {
    fetch(
	'http://localhost/api/arena/seek/',
	{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              locationTitle: formData.value
            })
          })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            state: 'game'
	  });
        },
        (error) => {
          this.setState({
	      state: 'choose_location'
          });
        }
      )
  }

  
  tick() {
    fetch('http://localhost/api/arena/')
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
	      player_id: result['player_id'],
	      arena: result['arena'],
	      state: result['status'],
	      playerCoords: result['playerCoords']
          });
	  setTimeout(() => this.tick(), 5000);
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  onMapLoad = map => {
      this.setstate({map: map});
  }

  render() {
    return (
	<div>
	<Container>
	  <Row>
	    <Col>
		<header className="App-header">
		  <img src={logo} className="App-logo" alt="logo" />
		</header>
	    </Col>
	  </Row>
	  <Row>
	    <Col xs="10" s="10" m="10" lg="10">
	      <div className="Map-container" > 
	        <GMaps coords={this.state.playerCoords} />
	      </div>
	    </Col>
	    <Col xs="2" s="2" m="2" lg="2">
	    </Col>
	  </Row>
	  <Row>
	    <Col>
		<InlineForm handleSubmit={this.makeTurn} />
                <Table rowData={this.state.arena} />
	    </Col>
	  </Row>
	</Container>
	<LocationModal show={(this.state.state === 'choose_location')} handleSubmit={this.joinArena} />
	<WaitingModal show={(this.state.state === 'wait')} tableData={ this.state.arena } />
	</div>
    );
  }
}
  
export default App;
