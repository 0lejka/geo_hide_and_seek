import React, { Component } from 'react';
import './Table.css';

class Table extends Component {
  state = {
    rows: []
  }

  render() {
    const rows = this.props.rowData.map((row, index) => {
      return (
        <tr key={index}>
          <td>{row.player_id}</td>
          <td>{row.distance}</td>
        </tr>
      )
    });

      return (
        <table className="table">
          <thead className="">
            <tr>
	        <th>player</th>
                <th>distance</th>
	      </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      );
  }
}

export default Table;

